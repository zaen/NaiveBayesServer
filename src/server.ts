import express = require('express')
import { Router } from "express";
import { MongoClient } from "mongodb";
let cors = require('cors')
// let bodyParser = require('body-parser')

// Application
let app = express()
let api = Router()


// Use CORS
app.use(cors())


// Routers
app.use('/api', api)


// Wildcard
app.get('*', (req, res, next) => {
    res.send('WHILD CARD')
})


// Listeting
app.listen(80, ()=> {
    console.log('Listening')
    console.log('Port 80')
})


// Routers
let uri: string = 'mongodb://127.0.0.1:8800'

api.get('/gets', (req, res, next) => {

    MongoClient.connect(uri)
    .then(client => {

        let db = client.db('naive')
        let col = db.collection('informations')

        return col.find()
        .sort({
            '_id': -1
        })
        .limit(1)
        .toArray()

    })
    .then(results => {

        res.send(results[0])

    })
    .catch(err => {
        console.log(err)
    })

})
