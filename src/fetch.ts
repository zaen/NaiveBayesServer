import { cloningReplies, cloningTweets } from "./lib/cloning";

let count = 100

// Tweets Replies
{
    
    // Ganjar Pranowo
    cloningReplies({
        dbName: "naive",
        collection: "replies_ganjar",
        collection_info: "replies_infoganjar",
        count: count,
        screen_name: "ganjarpranowo"
    })


    // Sudirman SaidS
    cloningReplies({
        dbName: "naive",
        collection: "replies_sudirman",
        collection_info: "replies_infosudirman",
        count: count,
        screen_name: "sudirmansaid"
    })

}


// Tweets
{

    // Ganjar Pranowo
    cloningTweets({
        dbName: "naive",
        collection: "tweets_ganjar",
        collection_info: "tweets_infoganjar",
        count: count,
        screen_name: "ganjarpranowo"
    })

    // Sudirman Said
    cloningTweets({
        dbName: "naive",
        collection: "tweets_sudirman",
        collection_info: "tweets_infosudirman",
        count: count,
        screen_name: "sudirmansaid"
    })

}
