let stemmer = require('akarata')
import { readFileSync } from "fs";

export class Stemmer{

    // Member Variable
    private stopWords: string[] = [ ]
    

    // Public Functions
    constructor(pathFileDictionary: string, stopwords?: string[]){

        let dic  = JSON.parse(
            readFileSync(
                pathFileDictionary, { encoding: 'UTF-8' }
            )
        )
        this.stopWords = dic.stopwords
        if (stopwords) {
            this.stopWords.push(...stopwords)
        }
    }

    normalize(sentences: string): string[]{
        return this.tokenAndStem(this.toWords(sentences))
    }

    // Private Functions
    private toWords(sentences: string): string[]{
        
        let tempWords = sentences.toLowerCase().split(' ')
        let words: string[] = [ ]
        let length = tempWords.length

        for (let i = 0; i < length; i++) {
            if(tempWords[i].length) words.push(tempWords[i])
        }

        return words
    }

    private tokenAndStem(words: string[]): string[]{

        let stemmedTokens: string[] = []
        let length = words.length
        
        for (let i = 0; i < length; i++) {

            if(this.stopWords.indexOf(words[i]) == -1){
                stemmedTokens.push(stemmer.stem(words[i]))
            }

        }
        return stemmedTokens
    }
}
