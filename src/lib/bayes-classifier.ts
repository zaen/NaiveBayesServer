import { Stemmer } from "./stemmer";
import { readFileSync } from "fs";


export class BayesClassifier {

    // Member Variable
    /**
     * Collection of added documents
     * Each document containin the class and array of token
     */
    private docs: { value: string[], label: string } [] = []

    /**
     * Index of the last added document
     */
    private count: number = 0

    /**
     * A map of every class features
     */
    private features: { [x: string]: any} = {}

    /**
     * A map containing each class and associated features
     * Each class has a map containing a feature index and the count of feature and the count of feature appearances for that class.
     */
    private classFeatures: { [x: string]: any} = {}

    /**
     * Keep track of how many features in each class
     */
    private classTotals: { [x: string]: any} = {}

    /**
     * Number of examples trained
     */
    private totalExamples: number = 1

    /* Additive smoothing to eliminate zeros when summing features,
    * in cases where no features are found in the document.
    * Used as a fail-safe to always return a class.
    * http://en.wikipedia.org/wiki/Additive_smoothing
    */
    private smoothing:number = 1;




    // Constructor
    constructor(private stemmer: Stemmer, pathNaive?: string) {

        if(pathNaive){
            
            let naiveDocs: any[] = JSON.parse(
                readFileSync(pathNaive, {encoding: 'UTF-8'})
            )

            let length = naiveDocs.length

            for (let i = 0; i < length; i++) {
                
                let label = naiveDocs[i].label
                let values = naiveDocs[i].values
                let lengthVals = values.length

                for (let index = 0; index < lengthVals; index++) {
                    
                    this.addDocument(values[i], label)

                }

            }

        }

    }


    // Functions
    addDocument(doc: string, label: string){

        let stemmedDoc = this.stemmer.normalize(doc)
        this.docs.push({ value: stemmedDoc, label: label })

        // Add token (feature) to features map
        let length = stemmedDoc.length
        for (let i = 0; i < length; i++) {
            this.features[stemmedDoc[i]] = 1
        }

    }

    addDocuments(docs: string[], label: string){ 

        let length = docs.length
        for (let i = 0; i < length; i++) {
            this.addDocument(docs[i], label)
        }

    }

    classify(doc: string): string{
        let classifications = this.getClassifications(doc)
        return classifications[0].label
    }

    train(){
        let length = this.docs.length
        for (let i = 0; i < length; i++) {
            let features = this.docToFeatures(this.docs[i].value.join(' '))
            this.addExample(features, this.docs[i].label)
            this.count++
        }
    }


    // Private Functions

        /**
     * return an array 0, 1
     * 1 if feature in the document
     * 0 if feature not in the document
     * @param doc - document
     * @return {number[]} features
     */
    private docToFeatures(doc: string): number[]{

        let features: number[] = []
        let stemmedDoc = this.stemmer.normalize(doc)

        for (const feature in this.features) {
            features.push(Number(!!~stemmedDoc.indexOf(feature)))
        }

        return features
    }

    private addExample(docsFeatures: number[], label: string){

        if(!this.classFeatures[label]){
            this.classFeatures[label] = {}
            this.classTotals[label] = 1
        }

        this.totalExamples++

        let i = docsFeatures.length
        this.classTotals[label]++
        while (i--) {
            if(docsFeatures[i]){
                if(this.classFeatures[label][i]){
                    this.classFeatures[label][i]++
                }else{
                    this.classFeatures[label][i] = 1 + this.smoothing
                }   
            }
        }

    }

    /**
     * probabilityOfClass
     * @param {array|string} docFeatures - document features
     * @param {string} label - class
     * @return probability;
     * @desc
     * calculate the probability of class for the document.
     *
     * Algorithm source
     * http://en.wikipedia.org/wiki/Naive_Bayes_classifier
     *
     * P(c|d) = P(c)P(d|c)
     *          ---------
     *             P(d)
     *
     * P = probability
     * c = class
     * d = document
     *
     * P(c|d) = Likelyhood(class given the document)
     * P(d|c) = Likelyhood(document given the classes).
     *     same as P(x1,x2,...,xn|c) - document `d` represented as features `x1,x2,...xn`
     * P(c) = Likelyhood(class)
     * P(d) = Likelyhood(document)
     *
     * rewritten in plain english:
     *
     * posterior = prior x likelyhood
     *             ------------------
     *                evidence
     *
     * The denominator can be dropped because it is a constant. For example,
     * if we have one document and 10 classes and only one class can classify
     * document, the probability of the document is the same.
     *
     * The final equation looks like this:
     * P(c|d) = P(c)P(d|c)
     */
    private probabilityOfClass(docsFeatures: number[], label: string): number{
        let count = 0
        let prob = 0

        let i = docsFeatures.length

        while (i--) {
            
            // Process if feature collection
            if (docsFeatures[i]) {
                
                // The number of occurances of the document feature in class
                count = this.classFeatures[label][i] || this.smoothing

                /* This is the `P(d|c)` part of the formula.
                * How often the class occurs. We simply count the relative
                * feature frequencies in the corpus (document body).
                *
                * We divide the count by the total number of features for the class,
                * and add it to the probability total.
                * We're using Natural Logarithm here to prevent Arithmetic Underflow
                * http://en.wikipedia.org/wiki/Arithmetic_underflow
                */
                prob += Math.log(count / this.classTotals[label])
            }

        }

        /*
        * This is the `P(c)` part of the formula.
        *
        * Divide the the total number of features in class by total number of all features.
        */
        let featureRatio = this.classTotals[label] / this.totalExamples
        
        /**
         * probability of class given document = P(d|c)P(c)
         */
        prob = featureRatio * Math.exp(prob)

        return prob
    }

    
    private getClassifications(doc: string): {value: number, label: string} [] {

        let labels: {value: number, label: string} [] = []

        for (const className in this.classFeatures) {
            
            labels.push({
                label: className,
                value: this.probabilityOfClass(this.docToFeatures(doc), className)
            })

        }

        return labels.sort((x, y) => {
            return y.value - x.value
        })
    }

}
